import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../ui/user/view/user_page.dart';


class StartBlocApp extends StatelessWidget {
  const StartBlocApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(          // Remove the const from here
      title: 'Startup User Information',
      home: UserPage(),// ... to here.
    );
  }
}
