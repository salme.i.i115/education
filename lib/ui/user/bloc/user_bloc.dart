import 'package:education_app/ui/user/bloc/user_events.dart';
import 'package:education_app/ui/user/bloc/user_state.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../api/rest_users_api.dart';

class UserBloc extends Bloc<UserEvents, UserState> {
  UserBloc() : super(UserState()) {
    on<GetUserEvents>(_onGetUser);
    on<GeJobUsersEvents>(_onGetJobsUser);
    on<GeUserHttpEvents>(_onGetUserInfo);
  }

  _onGetUser(GetUserEvents events, Emitter<UserState> emit) async {
    emit(state.copyWith(isLoading: true));
    await Future.delayed(Duration(seconds: 1));
    emit(state.copyWith(users: _createUsersList()));
  }

  _onGetJobsUser(GeJobUsersEvents events, Emitter<UserState> emit) async {
    emit(state.copyWith(isLoading: true));
    await Future.delayed(Duration(seconds: 1));
    emit(state.copyWith(jobUsers: _createJobUsers()));
  }

  _onGetUserInfo(GeUserHttpEvents events, Emitter<UserState> emit) async {
    emit(state.copyWith(isLoading: true));
    var client = RestClientUsers(Dio(BaseOptions(contentType: "application/json")));
    var response = await client.getUsers();
    emit(state.copyWith(usersInfo: response));
  }

  List<User> _createUsersList() => [
        User(id: "1", name: "Ibraim"),
        User(id: "2", name: "Aivaz"),
        User(id: "3", name: "Azim"),
        User(id: "4", name: "Nafe"),
        User(id: "5", name: "Remzi"),
        User(id: "6", name: "Artur")
      ];

  List<Job> _createJobUsers() => [
    Job(id: "1", jobName: "programmer"),
    Job(id: "2", jobName: "mechanic"),
    Job(id: "3", jobName: "cook"),
    Job(id: "4", jobName: "cook"),
    Job(id: "5", jobName: "mechanic"),
    Job(id: "6", jobName: "dentist")
  ];
}
