// abstract class UserState {}
//
// class UserInitial extends UserState {}
//
// class UserLoadedState extends UserState {
//
// }
//
// class UserLoadingState extends UserState {
//   final List<User> users;
//
//   UserLoadingState(this.users);
// }

import '../../../api/rest_users_api.dart';

class UserState {
  final List<User> users;
  final List<Job> jobUsers;
  final bool isLoading;
  final List<UserInfo> usersInfo;

  UserState(
      {this.users = const [],
      this.jobUsers = const [],
      this.isLoading = false,
      this.usersInfo = const []});

  UserState copyWith(
      {List<User>? users,
      List<Job>? jobUsers,
      isLoading = false,
      List<UserInfo>? usersInfo}) {
    return UserState(
        users: users ?? this.users,
        jobUsers: jobUsers ?? this.jobUsers,
        isLoading: isLoading,
        usersInfo: usersInfo ?? this.usersInfo);
  }
}

class User {
  final String id;
  final String name;

  User({required this.id, required this.name});
}

class Job {
  final String id;
  final String jobName;

  Job({required this.id, required this.jobName});
}
