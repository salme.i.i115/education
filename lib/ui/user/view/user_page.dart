import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../counter/bloc/counter_bloc.dart';
import '../../counter/bloc/counter_events.dart';
import '../bloc/user_bloc.dart';
import '../bloc/user_events.dart';
import '../bloc/user_state.dart';

class UserPage extends StatelessWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final blocCounter = CounterBloc();
    final blocUsers = UserBloc();
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double deviceHeight = MediaQuery.of(context).size.height;

    return MultiBlocProvider(
        providers: [
          BlocProvider<CounterBloc>(
            create: (context) => blocCounter,
          ),
          BlocProvider<UserBloc>(
            create: (context) => blocUsers,
          )
        ],
        child: Scaffold(
          appBar: AppBar(
            title: Text("Bloc app"),
            centerTitle: true,
          ),
          resizeToAvoidBottomInset: false,
          body: Container(
            child: Column(
              children: [
                // BlocBuilder<CounterBloc, int>(
                //     bloc: blocCounter,
                //     builder: (context, state) {
                //       return Text(
                //         state.toString(),
                //         style: TextStyle(fontSize: 25),
                //       );
                //     }),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // IconButton(
                    //   onPressed: () {
                    //     blocCounter.add(CounterIncEvents());
                    //   },
                    //   iconSize: 20,
                    //   icon: Icon(Icons.plus_one),
                    // ),
                    // SizedBox(width: 8),
                    // IconButton(
                    //   onPressed: () {
                    //     blocCounter.add(CounterDecEvents());
                    //   },
                    //   iconSize: 20,
                    //   icon: Icon(Icons.exposure_minus_1),
                    // ),
                    // SizedBox(width: 8),
                    IconButton(
                      onPressed: () {
                        blocUsers.add(GeUserHttpEvents());
                      },
                      iconSize: 20,
                      icon: Icon(Icons.person),
                    ),
                    // SizedBox(width: 8),
                    // IconButton(
                    //   onPressed: () {
                    //     blocUsers.add(GeJobUsersEvents());
                    //   },
                    //   iconSize: 20,
                    //   icon: Icon(Icons.add_card),
                    // ),
                  ],
                ),
                SizedBox(height: 30),
                BlocBuilder<UserBloc, UserState>(
                    bloc: blocUsers,
                    builder: (context, state) {
                      final users = state.users;
                      final jobs = state.jobUsers;
                      final usersInfo = state.usersInfo;
                      if (usersInfo.isNotEmpty) {
                        return Container(
                            width: deviceWidth,
                            height: 500,
                            child: Padding(
                              padding: const EdgeInsets.all(32.0),
                              child: CustomScrollView(slivers: <Widget>[
                                SliverList(
                                  delegate: SliverChildBuilderDelegate(
                                    (BuildContext context, int index) {
                                      // return Column(
                                      //   mainAxisSize: MainAxisSize.max,
                                      //   crossAxisAlignment:
                                      //       CrossAxisAlignment.start,
                                      //   children: [
                                      //     Text(
                                      //       "User id: ${usersInfo[index].userId.toString()}",
                                      //       style: TextStyle(
                                      //           fontWeight: FontWeight.bold,
                                      //           fontSize: 20),
                                      //     ),
                                      //     SizedBox(
                                      //       height: 8,
                                      //     ),
                                      //     Text(
                                      //       "Id: ${usersInfo[index].id.toString()}",
                                      //       style: TextStyle(
                                      //           fontWeight: FontWeight.bold,
                                      //           fontSize: 20),
                                      //     ),
                                      //     SizedBox(
                                      //       height: 8,
                                      //     ),
                                      //     Text(
                                      //       "Title: ${usersInfo[index].title.toString()}",
                                      //       style: TextStyle(fontSize: 14),
                                      //     ),
                                      //     SizedBox(
                                      //       height: 8,
                                      //     ),
                                      //     Text(
                                      //       "Body: ${usersInfo[index].body.toString()}",
                                      //       style: TextStyle(fontSize: 14),
                                      //     ),
                                      //   ],
                                      // );
                                      return SingleChildScrollView(
                                          child: Text(
                                              "body ${usersInfo[index].body}"));
                                    },
                                  ),
                                )
                              ]),
                            ));
                        // itemCount: usersInfo.length,
                        // separatorBuilder: (_, __) => Divider(
                        //   color: Colors.grey,
                        // ),
                        // padding: EdgeInsets.all(8),
                        //     itemBuilder: (context, index) {
                        //       return Container(
                        //         margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
                        //         child: Column(
                        //           crossAxisAlignment: CrossAxisAlignment.start,
                        //           children: [
                        //             Text(
                        //               "User id: ${usersInfo[index].userId.toString()}",
                        //               style: TextStyle(
                        //                   fontWeight: FontWeight.bold,
                        //                   fontSize: 20),
                        //             ),
                        //             SizedBox(
                        //               height: 8,
                        //             ),
                        //             Text(
                        //               "Id: ${usersInfo[index].id.toString()}",
                        //               style: TextStyle(
                        //                   fontWeight: FontWeight.bold,
                        //                   fontSize: 20),
                        //             ),
                        //             SizedBox(
                        //               height: 8,
                        //             ),
                        //             Text(
                        //               "Title: ${usersInfo[index].title.toString()}",
                        //               style: TextStyle(fontSize: 14),
                        //             ),
                        //             SizedBox(
                        //               height: 8,
                        //             ),
                        //             Text(
                        //               "Body: ${usersInfo[index].body.toString()}",
                        //               style: TextStyle(fontSize: 14),
                        //             ),
                        //           ],
                        //         ),
                        //       );
                        //     },
                        //   ),
                        // );
                      } else {
                        return Column(
                          children: [
                            if (state.isLoading) CircularProgressIndicator(),
                            if (users.isNotEmpty)
                              ...users.map(
                                  (e) => Text("id:${e.id}   name:${e.name}")),
                            if (jobs.isNotEmpty)
                              ...jobs.map((e) =>
                                  Text("id:${e.id}   job name:${e.jobName}")),
                            if (usersInfo.isNotEmpty)
                              Container(
                                child: ListView.separated(
                                  itemCount: usersInfo.length,
                                  separatorBuilder: (_, __) => Divider(
                                    color: Colors.grey,
                                  ),
                                  padding: EdgeInsets.all(8),
                                  itemBuilder: (context, index) {
                                    return Container(
                                      margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "User id: ${usersInfo[index].userId.toString()}",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20),
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Text(
                                            "Id: ${usersInfo[index].id.toString()}",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20),
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Text(
                                            "Title: ${usersInfo[index].title.toString()}",
                                            style: TextStyle(fontSize: 14),
                                          ),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Text(
                                            "Body: ${usersInfo[index].body.toString()}",
                                            style: TextStyle(fontSize: 14),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              )

                            // if(state is UserLoadedState) const CircularProgressIndicator(),
                            // if(state is UserLoadingState)...state.users.map((e) => Text("id:${e.id}   name:${e.name}")),
                          ],
                        );
                      }
                    })
              ],
            ),
          ),
        ));
  }
}
