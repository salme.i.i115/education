

import 'package:flutter_bloc/flutter_bloc.dart';

import 'counter_events.dart';

class CounterBloc extends Bloc<CounterEvents, int> {
  CounterBloc() : super(5) {
    on<CounterIncEvents>(_onIncrement);
    on<CounterDecEvents>(_onDecrement);
  }

  _onIncrement(CounterEvents events, Emitter<int> emit) {
    emit(state + 1);
  }

  _onDecrement(CounterEvents events, Emitter<int> emit) {
    if (state == 0) return;
    emit(state - 1);
  }
}


