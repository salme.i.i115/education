import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/counter_bloc.dart';
import '../bloc/counter_events.dart';

class CounterPage extends StatelessWidget {
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = CounterBloc();

    return BlocProvider<CounterBloc>(
      create: (context) => bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Bloc app"),
          centerTitle: true,
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<CounterBloc, int>(
                  bloc: bloc,
                  builder: (context, state) {
                    return Text(
                      state.toString(),
                      style: TextStyle(fontSize: 25),
                    );
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      bloc.add(CounterIncEvents());
                    },
                    iconSize: 20,
                    icon: Icon(Icons.plus_one),
                  ),
                  SizedBox(width: 16),
                  IconButton(
                    onPressed: () {
                      bloc.add(CounterDecEvents());
                    },
                    iconSize: 20,
                    icon: Icon(Icons.exposure_minus_1),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
