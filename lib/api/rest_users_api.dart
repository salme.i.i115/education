import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';


part 'rest_users_api.g.dart';

@RestApi(baseUrl: "https://jsonplaceholder.typicode.com/todos/1")
abstract class RestClientUsers {
  factory RestClientUsers(Dio dio, {String baseUrl}) = _RestClientUsers;

  @GET("/posts")
  Future<List<UserInfo>> getUsers();
}


@JsonSerializable()
class UserInfo {
  int? userId;
  int? id;
  String? title;
  String? body;

  UserInfo({this.userId, this.id, this.title, this.body});

  factory UserInfo.fromJson(Map<String, dynamic> json) => _$UserInfoFromJson(json);
  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
